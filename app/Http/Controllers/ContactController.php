<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;
use DB;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $contacts = DB::table('contacts')->get();
        return view('contacts.index', ['contacts'=> $contacts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('contacts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $inputs = $request->except('_token');

        $contact = new Contact();

        $contact->firstname = $request->input('firstname');
        $contact->lastname = $request->input('lastname');
        $contact->number = $request->input('number');
        $contact->email = $request->input('email');
        $contact->save();

        return redirect('contacts');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $contacts = Contact::find($id);
        return view('contacts.edit', ['contacts' => $contacts]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $inputs = $request->except('_token','_method');
        $contact = Contact::find($id);
        foreach($inputs as $key => $value){
            $contact->$key = $value;
        }
        $contact->save();

        return redirect('contacts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $contact = Contact::find($id);
        $contact->delete();

        return redirect('contacts');
    }
    public function search(Request $request)
    {
        $search = $request->get('search');
        $contacts = DB::table('contacts')->where('firstname', 'like', '%'.$search.'%');
        $contacts = DB::table('contacts')->where('lastname', 'like', '%'.$search.'%');
    return view('contacts.index', ['contacts'=>$contacts]);

    }
}

    
