
@extends('layouts.template')

@section('title', 'Mes Contacts')

@section('content')
<div class="container mt-4">
  <div class="row" style="margin-right:70px">
    <div class="col py-4 px-lg-5 border bg-light text-center">Prénom</div>
    <div class="col py-3 px-lg-5 border bg-light text-center py-4">Nom</div>
    <div class="col py-3 px-lg-5 border bg-light text-center py-4">Téléhpone</div>
    <div class="col py-3 px-lg-5 border bg-light text-center py-4">Email</div>
  </div>
</div>
@foreach($contacts as $contact)

<div class="container mt-4">
        <div class="row  mb-2">
            <div class="col py-4 px-lg-5 border bg-light text-center">{{$contact->firstname}}</div>
            <div class="col py-3 px-lg-5 border bg-light text-center py-4">{{$contact->lastname}}</div>
            <div class="col py-3 px-lg-5 border bg-light text-center py-4">{{$contact->number}}</div>
            <div class="col py-3 px-lg-5 border bg-light text-center py-4">{{$contact->email}}</div>
          <div class="row d-flex ml-2" style="height: 40px; margin-top:25px;">
            <a class="btn btn-primary" href="{{route('contacts.edit', ['contact' => $contact->id])}}" role="button">
              <i class="fas fa-pen"></i>
            </a>
          </div>
          <form method="POST" action="{{route('contacts.destroy', ['contact' => $contact->id])}}">
            @method('DELETE')
            @csrf
            <div class="row  ml-4" style="height: 40px; margin-top:25px;">
              <button type="submit" class="btn btn-primary">
                <i class="fas fa-trash"></i>
              </button>
            </div>
          </form>
        </div>
    </div>
@endforeach
<div class="row">
      <div class="mx-auto">
        <a class="btn btn-primary" href="{{route('contacts.create', ['contact' => $contact->id])}}" role="button">
          <i class="fas fa-plus-circle"></i>
        </a>
      <div>
    </div>
    

@endsection