@extends('layouts.template')

@section('title', 'Créer Contact')

@section('content')

<form class="ml-3 mr-3" method="POST" action="{{route('contacts.store')}}">
@csrf
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="firstname">Firstname</label>
      <input type="text" name ="firstname" class="form-control" id="firstname">
    </div>
    
    <div class="form-group col-md-6">
      <label for="lastname">LastName</label>
      <input type="text" name="lastname" class="form-control" id="lastname">
    </div>
  </div>

  <div class="form-group">
    <label for="tel">Number</label>
    <input type="tel" name="number" class="form-control" id="number">
  </div>

  <div class="form-group">
    <label for="email">email</label>
    <input type="email" name="email" class="form-control" id="email"></input>
  </div>
  <button type="submit" class="btn btn-primary" style="margin-left:50%;">créer</button>
</form>
@endsection