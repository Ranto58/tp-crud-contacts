@extends('layouts.template')

@section('title', 'Rechercher Contact')

@section('content')

<form class="ml-3 mr-3" method="POST" action="{{route('contacts.search', ['contact' => $contacts->id])}}">
@method('PUT')
@csrf
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="firstname">Firstname</label>
      <input type="text" name ="firstname" class="form-control" id="firstname"  value="{{$contacts->firstname}}">
    </div>
    
    <div class="form-group col-md-6">
      <label for="lastname">LastName</label>
      <input type="text" name="lastname" class="form-control" id="lastname" value="{{$contacts->lastname}}">
    </div>
  </div>
  <button type="submit" class="btn btn-primary" style="margin-left:50%;">Mettre à jour</button>
</form>