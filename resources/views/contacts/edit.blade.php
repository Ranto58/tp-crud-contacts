@extends('layouts.template')

@section('title', 'Modifier Contact')

@section('content')

<form class="ml-3 mr-3" method="POST" action="{{route('contacts.update', ['contact' => $contacts->id])}}">
@method('PUT')
@csrf
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="firstname">Firstname</label>
      <input type="text" name ="firstname" class="form-control" id="firstname"  value="{{$contacts->firstname}}">
    </div>
    
    <div class="form-group col-md-6">
      <label for="lastname">LastName</label>
      <input type="text" name="lastname" class="form-control" id="lastname" value="{{$contacts->lastname}}">
    </div>
  </div>

  <div class="form-group">
    <label for="tel">Number</label>
    <input type="tel" name="number" class="form-control" id="number" value="{{$contacts->number}}">
  </div>

  <div class="form-group">
    <label for="email">email</label>
    <input type="email" name="email" class="form-control" id="email" value="{{$contacts->email}}"></input>
  </div>
  <button type="submit" class="btn btn-primary" style="margin-left:50%;">Mettre à jour</button>
</form>
@endsection